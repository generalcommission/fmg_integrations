import logging
from typing import Dict, List, Union

from backend.src.config import GC_GENERIC_ERROR_MESSAGE
from backend.src.providers.apollo.client import ApolloClient
from backend.src.providers.apollo.dtos import (
    OrganizationFragment,
    Employee,
    CurrentTechnology,
    Organization,
    JobOpening,
    EnrichedPerson,
    SocialFragment,
    Address
)


class ApolloExtractor:
    """Extract and structure data from apollo api

    API documentation= https=//apolloio.github.io/apollo-api-docs/
    """

    def __init__(self, api_client: ApolloClient, logger: logging.Logger):
        self.api_client = api_client
        self.log = logger

    def get_employees(self, domain: str, page: int = 1) -> Union[List[Employee], Dict]:
        try:
            response = self.api_client.get_employees(domain, page)
            raw_employees = response.get('people')
            structured_employees = self._get_structured_employees(raw_employees)
            employees_response = {
                'employees': structured_employees,
                'currentPage': response.get('pagination', {}).get('page'),
                'totalPages': response.get('pagination', {}).get('total_pages')
            }
            return employees_response

        except Exception as ex:
            self.log.exception(str(ex))
            return {
                'errors': GC_GENERIC_ERROR_MESSAGE
            }

    def get_enriched_person(
        self,
        first_name: str,
        last_name: str,
        org_name: str,
        domain: str,
        email: str
    ) -> Union[EnrichedPerson, Dict]:
        try:
            response = self.api_client.get_enriched_person(first_name, last_name, org_name, domain, email)
            raw_enriched_person = response.get("person")
            structured_enriched_person = self._get_enriched_person_dto(raw_enriched_person)
            return structured_enriched_person

        except Exception as ex:
            self.log.exception(str(ex))
            return {
                'errors': GC_GENERIC_ERROR_MESSAGE
            }

    def get_organization(self, domain: str) -> Union[Organization, Dict]:
        try:
            organization_response = self.api_client.get_organization(domain)
            raw_organization = organization_response["organization"]
            structured_organization = self._get_organization_dto(raw_organization)
            return structured_organization

        except Exception as ex:
            self.log.exception(str(ex))
            return {
                'errors': GC_GENERIC_ERROR_MESSAGE
            }
    
    def get_job_openings(self, domain: str) -> Union[List[JobOpening], Dict]:
        try:
            response = self.api_client.get_job_openings(domain)
            raw_job_openings = response.get('organization_job_postings')
            structured_job_openings = self._get_structured_job_openings(raw_job_openings)
            return structured_job_openings
        
        except Exception as ex:
            self.log.exception(str(ex))
            return {
                'errors': GC_GENERIC_ERROR_MESSAGE
            }

    def _get_structured_employees(self, employees: List) -> List[Employee]:
        employee_list = [self._get_employee_dto(employee) for employee in employees]
        return employee_list

    def _get_structured_job_openings(self, job_openings: List) -> List:
        job_opening_list = [self._get_job_opening_dto(job) for job in job_openings]
        return job_opening_list

    def _get_employee_dto(self, employee: Dict) -> Employee:
        employee_dto = Employee(
            firstName = employee.get('first_name'),
            lastName= employee.get('last_name'),
            fullName= employee.get('name'),
            socialUrls= self._get_social_fragment_dto(employee),
            jobTitle= employee.get('title'),
            address= self._get_address_dto(employee),
            taglineDescription= employee.get('headline'),
            organization= self._get_organization_fragment_dto(employee.get('organization', {}))
            )
        return employee_dto

    def _get_enriched_person_dto(self, enriched_person: Dict) -> EnrichedPerson:
        if enriched_person.get("organization"):
            enriched_person = EnrichedPerson(
                firstName=enriched_person.get('first_name'),
                lastName=enriched_person.get('last_name'),
                fullName=enriched_person.get('name'),
                socialUrls=self._get_social_fragment_dto(enriched_person),
                title=enriched_person.get('title'),
                email=enriched_person.get('email'),
                emailStatus=enriched_person.get('email_status'),
                address=self._get_address_dto(enriched_person),
                organization=self._get_organization_fragment_dto(enriched_person["organization"])
                )
        else:
            enriched_person = EnrichedPerson(
                firstName=enriched_person.get('first_name'),
                lastName=enriched_person.get('last_name'),
                fullName=enriched_person.get('name'),
                socialUrls=self._get_social_fragment_dto(enriched_person),
                title=enriched_person.get('title'),
                email=enriched_person.get('email'),
                emailStatus=enriched_person.get('email_status'),
                address=self._get_address_dto(enriched_person)
                )
        return enriched_person

    def _get_organization_dto(self, organization: Dict) -> Organization:
        organization = Organization(
            name=organization.get('name'),
            domain=organization.get('website_url'),
            blog=organization.get('blog_url'),
            socialUrls= self._get_social_fragment_dto(organization),
            phoneNumber=organization['primary_phone'].get('number'),
            languages=organization.get('languages'),
            alexaRanking=organization.get('alexa_ranking'),
            linkedinUid=organization.get('linkedin_uid'),
            stockSymbol=organization.get('publicly_traded_symbol'),
            stockExchange=organization.get('publicly_traded_exchange'),
            logoLocation=organization.get('logo_url'),
            crunchbaseUrl=organization.get('crunchbase_url'),
            marketCap=organization.get(''),
            industry=organization.get('industry'),
            descriptors=organization.get('keywords'),
            numberOfEmployees=organization.get('estimated_num_employees'),
            address=self._get_address_dto(organization),
            parentOrgIdentifier=organization.get('owned_by_organization_id'),
            suborganizations= [self._get_organization_fragment_dto(suborg) for suborg in organization['suborganizations']],
            numberOfSuborganizations=organization.get('num_suborganizations'),
            searchDescription=organization.get('seo_description'),
            shortDescription=organization.get('short_description'),
            annualRevenue=organization.get('revenue'),
            techStackNames=organization.get('technology_names'),
            currentTechnologies=[self._get_current_technology_dto(tech) for tech in organization['current_technologies']]
            )
        return organization

    def _get_job_opening_dto(self, job_opening: Dict) -> JobOpening:
        job_opening = JobOpening(
            title=job_opening.get('title'),
            listingUrl=job_opening.get('url'),
            address=self._get_address_dto(job_opening),
            timePosted=job_opening.get('posted_at')
            )
        return job_opening

    def _get_social_fragment_dto(self, object_with_socials: Dict) -> SocialFragment:
        social_fragment = SocialFragment(
            linkedinUrl=object_with_socials.get('linkedin_url'),
            photoUrl=object_with_socials.get('photo_url'),
            twitterUrl=object_with_socials.get('twitter_url'),
            facebookUrl=object_with_socials.get('facebook_url'),
            githubUrl=object_with_socials.get('github_url'),
            angellistUrl=object_with_socials.get('angellist_url')
            )
        return social_fragment

    def _get_organization_fragment_dto(self, organization_fragment: Dict) -> OrganizationFragment:
        organization_fragment = OrganizationFragment(
            name=organization_fragment.get('name'),
            domain=organization_fragment.get('website_url')
            )
        return organization_fragment
    
    def _get_current_technology_dto(self, current_technology: Dict) -> CurrentTechnology:
        current_technology = CurrentTechnology(
            uid=current_technology.get('uid'),
            name=current_technology.get('name'),
            category=current_technology.get('category')
            )
        return current_technology
        
    def _get_address_dto(self, address_object: Dict) -> Address:
        address_dto = Address(
            address=address_object.get('raw_address'),
            street=address_object.get('street_address'),
            city=address_object.get('city'),
            state=address_object.get('state'),
            country=address_object.get('country'),
            zipcode=address_object.get('postal_code')
        )
        return address_dto