import logging

from backend.factories import get_configured_logger
from backend.src.config import APOLLO_KEY
from backend.src.providers.apollo.client import ApolloClient
from backend.src.providers.apollo.extractor import ApolloExtractor


def get_apollo_extractor() -> ApolloExtractor:
    logger = get_configured_logger(logging.INFO)
    client = ApolloClient(APOLLO_KEY, logger)
    extractor = ApolloExtractor(client, logger)
    return extractor