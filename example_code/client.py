import logging
from typing import Dict, Optional

import requests

from backend.src.utilities.decorators import apply_request_backoff


class ApolloClient:
    """Fetch data from Apollo api

    API documentation: https://apolloio.github.io/apollo-api-docs/
    """

    API_VERSION = 'v1'
    BASE_URL = f'https://api.apollo.io/{API_VERSION}'
    SECONDS_TO_BACKOFF = 1

    def __init__(self, api_key: str, logger: logging.Logger):
        self.api_key = api_key
        self.logger = logger

    def get_employees(self, domain: str, page: int) -> Dict:
        url = f'{self.BASE_URL}/mixed_people/search'
        params = {
            'api_key': self.api_key,
            'q_organization_domains': [domain],
            'page' : page
            }
        employees = self._make_request(url, params)
        return employees

    def get_enriched_person(
        self,
        first_name: str,
        last_name: str,
        organization_name: Optional[str],
        domain: Optional[str],
        email: Optional[str]
    ) -> Dict:
        url = f'{self.BASE_URL}/people/match'
        params = {
            'api_key' : self.api_key,
            "first_name": first_name,
            "last_name": last_name,
            "organization_name": organization_name,
            "domain": domain,
            "email": email
            }
        enriched_person = self._make_request(url, params)
        return enriched_person

    def get_organization(self, domain: str) -> Dict:
        url = f'{self.BASE_URL}/organizations/enrich'
        params = {
            'api_key' : self.api_key,
            'domain' : domain
            }
        organization = self._make_request(url, params)
        return organization
    
    def get_job_openings(self, domain: str) -> Dict:
        identifier = self._get_org_identifier(domain)
        url = f'{self.BASE_URL}/organizations/{identifier}/job_postings'
        params = {
            'api_key' : self.api_key
            }
        job_openings = self._make_request(url, params)
        return job_openings

    def _get_org_identifier(self, domain: str) -> str:
        organization = self.get_organization(domain)
        identifier = organization["organization"]["id"]
        return identifier

    @apply_request_backoff(SECONDS_TO_BACKOFF)
    def _make_request(self, url: str, params: Dict) -> Dict:
        headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache'
            }
        response = requests.get(url=url, params=params, headers=headers)
        response.raise_for_status()
        return response.json()