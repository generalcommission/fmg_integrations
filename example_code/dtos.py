from typing import List, Optional

from dataclasses import dataclass


@dataclass
class OrganizationFragment:
    name: Optional[str]
    domain: Optional[str]


@dataclass()
class Address:
    zipcode: Optional[str] = None
    country: Optional[str] = None
    city: Optional[str] = None
    state: Optional[str] = None
    street: Optional[str] = None
    address: Optional[str] = None


@dataclass
class SocialFragment:
    linkedinUrl: Optional[str]
    photoUrl: Optional[str]
    twitterUrl: Optional[str]
    facebookUrl: Optional[str]
    githubUrl: Optional[str]
    angellistUrl: Optional[str]


@dataclass
class Employee:
    firstName: Optional[str]
    lastName: Optional[str]
    fullName: Optional[str]
    jobTitle: Optional[str]
    address: Optional[Address]
    socialUrls: Optional[SocialFragment]
    taglineDescription: Optional[str]
    organization: Optional[OrganizationFragment]


@dataclass
class Employees:
    employees: [Employee]
    currentPage: int
    totalPages: int
    errors: str


@dataclass
class EnrichedPerson:
    firstName: Optional[str]
    lastName: Optional[str]
    fullName: Optional[str]
    socialUrls: Optional[SocialFragment]
    title: Optional[str]
    email: Optional[str]
    emailStatus: Optional[str]
    address: Optional[Address]
    organization: Optional[OrganizationFragment] = None
    

@dataclass 
class CurrentTechnology:
    uid: Optional[str]
    name: Optional[str]
    category: Optional[str]


@dataclass
class Organization:
    name: Optional[str]
    domain: Optional[str]
    blog: Optional[str]
    socialUrls: Optional[SocialFragment]
    phoneNumber: Optional[str]
    languages: Optional[List]
    alexaRanking: Optional[int]
    linkedinUid: Optional[int]
    stockSymbol: Optional[str]
    stockExchange: Optional[str]
    logoLocation: Optional[str]
    crunchbaseUrl: Optional[str]
    marketCap: Optional[str]
    industry: Optional[str]
    descriptors: Optional[List]
    numberOfEmployees: Optional[int]
    address: Optional[Address]
    parentOrgIdentifier: Optional[str]
    suborganizations: [Optional[OrganizationFragment]]
    numberOfSuborganizations: Optional[int]
    searchDescription: Optional[str]
    shortDescription: Optional[str]
    annualRevenue: Optional[str]
    techStackNames: Optional[List]
    currentTechnologies: [Optional[CurrentTechnology]]


@dataclass
class JobOpening:
    title: Optional[str]
    listingUrl: Optional[str]
    address: Optional[Address]
    timePosted: Optional[str]