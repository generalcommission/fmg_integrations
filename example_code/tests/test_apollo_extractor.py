from unittest import TestCase

from mock import Mock

from backend.src.config import GC_GENERIC_ERROR_MESSAGE
from backend.src.providers.apollo.dtos import (
    OrganizationFragment,
    Employee,
    CurrentTechnology,
    Organization,
    JobOpening,
    EnrichedPerson,
    SocialFragment,
    Address
)
from backend.src.providers.apollo.extractor import ApolloExtractor

EMPLOYEE_RESPONSE = {
    'people': [{
        'first_name': 'Teddy',
        'last_name': 'White',
        'name': 'Teddy White',
        'linkedin_url': 'test linkedin',
        'title': 'test_title',
        'city': 'San Francisco',
        'email_status': 'verified',
        'photo_url': 'test photo',
        'twitter_url': 'test twitter',
        'github_url': 'test github',
        'facebook_url': 'test facebook',
        'headline': 'test headline',
        'country': 'United States',
        'state': 'CA',
        'organization': {
            'name': 'Google',
            'website_url': 'http://www.google.com',
        }
    }],
    'pagination': {
        'page': 1,
        'total_pages': 10
    }
}

ORGANIZATION_RESPONSE = {
    'organization': {
        'id': '5e66b6381e05b4008c8331b8',
        'name': 'Apollo',
        'website_url': 'http://www.apollo.io',
        'blog_url': None,
        'angellist_url': None,
        'linkedin_url': 'http://www.linkedin.com/company/apolloio',
        'twitter_url': 'https://twitter.com/MeetApollo/',
        'facebook_url': 'https://www.facebook.com/MeetApollo/',
        'primary_phone': {},
        'languages': [],
        'alexa_ranking': 35558,
        'phone': None,
        'linkedin_uid': '18511550',
        'publicly_traded_symbol': None,
        'publicly_traded_exchange': None,
        'logo_url': 'https://zenprospect-production.s3.amazonaws.com/uploads/pictures/5f8a0d9ad1187e000139430e/picture',
        'crunchbase_url': None,
        'primary_domain': 'apollo.io',
        'persona_counts': {},
        'industry': 'computer software',
        'keywords': ['sales engagement', 'lead generation', 'predictive analytics', 'lead scoring', 'sales strategy', 'conversation intelligence', 'sales enablement', 'lead routing', 'sales development', 'and email engagement'],
        'estimated_num_employees': 45,
        'snippets_loaded': True,
        'industry_tag_id': '5567cd4e7369643b70010000',
        'retail_location_count': 0,
        'raw_address': '535 Mission St, Suite 1100, San Francisco, California 94105, US',
        'street_address': '535 Mission St',
        'city': 'San Francisco',
        'state': 'California',
        'postal_code': '94105',
        'country': 'United States',
        'owned_by_organization_id': None,
        'suborganizations': [],
        'num_suborganizations': 0,
        'seo_description': 'Apollo is an intelligent, data-first engagement platform that puts structured data at the core of your workflows to help you execute, analyze, and improve on your growth strategy.',
        'short_description': "Apollo is the unified engagement acceleration platform that gives reps the ability to dramatically increase their number of quality conversations and opportunities. Reps are empowered to do more than just conduct outreach, they learn who to target, how to reach out, and what to say at speed and scale. We help drive growth and success by providing the means for teams to discover and utilize their organization's unique best practices. \n\nBy working in a unified platform, reps and managers alike save hours of time each day, strategy changes are instantly scaled across the whole team, and managers can finally dig into data at each step of their pipeline to continually find new ways to improve. \n\nTeams get access to our database of 200+ million contacts with a built-in fully customizable Scoring Engine, full sales engagement stack, our native Account Playbook builder, and the industry's only custom deep analytics suite. Managers create and enforce order and process with the industry's most advanced Rules Engine.\n\nApollo is the foundation for your entire end-to-end sales strategy.\n\nJoin teams like inDesign, Eden, and Gympass to experience the future of sales today. Ready to join our crew? Email sales@apollo.io.",
        'total_funding': None,
        'total_funding_printed': None,
        'latest_funding_round_date': None,
        'latest_funding_stage': None,
        'funding_events': [],
        'technology_names': ['Cloudflare DNS', 'Rackspace MailGun', 'Gmail', 'Marketo', 'Google Apps', 'Microsoft Office 365', 'CloudFlare Hosting', 'Route 53', 'Zendesk', 'Stripe', 'Segment.io', 'Amplitude', 'Hubspot', 'MailChimp', 'Eloqua', 'Dropbox', 'eRecruit', 'Crelate', 'Front Desk Scheduling', 'ShareThis', 'AT Internet', 'Intercom', 'Act-On', 'Avangate', 'Eventbrite', 'Django', 'Inspectlet', 'Unreal Engine', 'PC Recruiter', 'PicReel', 'Intelligent Demand', 'Google Play', 'etouches', 'DoubleClick Conversion', 'Mixpanel', 'SilkRoad', 'Greenhouse.io', 'Bootstrap Framework', 'FullStory', 'Google Dynamic Remarketing', 'Lever', 'Google Tag Manager', 'iPerceptions', 'WordPress.org', 'Google Analytics', 'iTunes', 'Typekit', 'DoubleClick', 'Linkedin Login', 'Linkedin Widget', 'Wistia', 'Jobvite', 'Litmos', 'Amadesa', 'Ruby On Rails', 'New Relic', 'Nginx', 'Facebook Login (Connect)', 'Taleo', 'Ubuntu', 'Vidyard', 'Open AdStream (Appnexus)', 'Linkedin Marketing Solutions', 'Google Font API', 'Google AdWords Conversion', 'Zopim', 'Multilingual', 'Facebook Widget', 'iCIMS', 'Mobile Friendly', 'SearchDex', 'Facebook Custom Audiences'],
        'current_technologies': [{
            'uid': 'cloudflare_dns',
            'name': 'Cloudflare DNS',
            'category': 'Domain Name Services'
        }, {
            'uid': 'rackspace_mailgun',
            'name': 'Rackspace MailGun',
            'category': 'Email Delivery'
        }, {
            'uid': 'gmail',
            'name': 'Gmail',
            'category': 'Email Providers'
        }, {
            'uid': 'marketo',
            'name': 'Marketo',
            'category': 'Marketing Automation'
        }, {
            'uid': 'google_apps',
            'name': 'Google Apps',
            'category': 'Other'
        }, {
            'uid': 'office_365',
            'name': 'Microsoft Office 365',
            'category': 'Other'
        }, {
            'uid': 'cloudflare_hosting',
            'name': 'CloudFlare Hosting',
            'category': 'Hosting'
        }, {
            'uid': 'route_53',
            'name': 'Route 53',
            'category': 'Domain Name Services'
        }, {
            'uid': 'zendesk',
            'name': 'Zendesk',
            'category': 'Support and Feedback'
        }, {
            'uid': 'stripe',
            'name': 'Stripe',
            'category': 'Payments'
        }, {
            'uid': 'segment_io',
            'name': 'Segment.io',
            'category': 'Analytics and Tracking'
        }, {
            'uid': 'amplitude',
            'name': 'Amplitude',
            'category': 'Analytics and Tracking'
        }, {
            'uid': 'hubspot',
            'name': 'Hubspot',
            'category': 'Marketing Automation'
        }, {
            'uid': 'mailchimp',
            'name': 'MailChimp',
            'category': 'Email Marketing'
        }, {
            'uid': 'eloqua',
            'name': 'Eloqua',
            'category': 'Marketing Automation'
        }, {
            'uid': 'dropbox',
            'name': 'Dropbox',
            'category': 'Cloud Services'
        }, {
            'uid': 'erecruit',
            'name': 'eRecruit',
            'category': 'Recruitment'
        }, {
            'uid': 'crelate',
            'name': 'Crelate',
            'category': 'Recruitment'
        }, {
            'uid': 'front_desk_scheduling',
            'name': 'Front Desk Scheduling',
            'category': 'Appointment Scheduling'
        }, {
            'uid': 'sharethis',
            'name': 'ShareThis',
            'category': 'Social Sharing'
        }, {
            'uid': 'at_internet',
            'name': 'AT Internet',
            'category': 'Analytics and Tracking'
        }, {
            'uid': 'intercom',
            'name': 'Intercom',
            'category': 'Support and Feedback'
        }, {
            'uid': 'act-on',
            'name': 'Act-On',
            'category': 'Marketing Automation'
        }, {
            'uid': 'avangate',
            'name': 'Avangate',
            'category': 'Payments'
        }, {
            'uid': 'eventbrite',
            'name': 'Eventbrite',
            'category': 'Event Management'
        }, {
            'uid': 'django',
            'name': 'Django',
            'category': 'Frameworks and Programming Languages'
        }, {
            'uid': 'inspectlet',
            'name': 'Inspectlet',
            'category': 'Analytics and Tracking'
        }, {
            'uid': 'unreal_engine',
            'name': 'Unreal Engine',
            'category': 'Other'
        }, {
            'uid': 'pc_recruiter',
            'name': 'PC Recruiter',
            'category': 'Recruitment'
        }, {
            'uid': 'picreel',
            'name': 'PicReel',
            'category': 'Site and Cart Abandonment'
        }, {
            'uid': 'intelligent_demand',
            'name': 'Intelligent Demand',
            'category': 'Lead Generation Software'
        }, {
            'uid': 'google_play',
            'name': 'Google Play',
            'category': 'Widgets'
        }, {
            'uid': 'etouches',
            'name': 'etouches',
            'category': 'Event Management'
        }, {
            'uid': 'doubleclick_conversion',
            'name': 'DoubleClick Conversion',
            'category': 'Advertising Networks'
        }, {
            'uid': 'mixpanel',
            'name': 'Mixpanel',
            'category': 'Analytics and Tracking'
        }, {
            'uid': 'silkroad',
            'name': 'SilkRoad',
            'category': 'Recruitment'
        }, {
            'uid': 'greenhouse_io',
            'name': 'Greenhouse.io',
            'category': 'Recruitment'
        }, {
            'uid': 'bootstrap_framework',
            'name': 'Bootstrap Framework',
            'category': 'CSS and JavaScript Libraries'
        }, {
            'uid': 'fullstory',
            'name': 'FullStory',
            'category': 'Other'
        }, {
            'uid': 'google_dynamic_remarketing',
            'name': 'Google Dynamic Remarketing',
            'category': 'Retargeting'
        }, {
            'uid': 'lever',
            'name': 'Lever',
            'category': 'Recruitment'
        }, {
            'uid': 'google_tag_manager',
            'name': 'Google Tag Manager',
            'category': 'Tag Management'
        }, {
            'uid': 'iperceptions',
            'name': 'iPerceptions',
            'category': 'Support and Feedback'
        }, {
            'uid': 'wordpress_org',
            'name': 'WordPress.org',
            'category': 'CMS'
        }, {
            'uid': 'google_analytics',
            'name': 'Google Analytics',
            'category': 'Analytics and Tracking'
        }, {
            'uid': 'itunes',
            'name': 'iTunes',
            'category': 'Widgets'
        }, {
            'uid': 'typekit',
            'name': 'Typekit',
            'category': 'Fonts'
        }, {
            'uid': 'doubleclick',
            'name': 'DoubleClick',
            'category': 'Ad Servers'
        }, {
            'uid': 'linkedin_login',
            'name': 'Linkedin Login',
            'category': 'Social Login'
        }, {
            'uid': 'linkedin_widget',
            'name': 'Linkedin Widget',
            'category': 'Other'
        }, {
            'uid': 'wistia',
            'name': 'Wistia',
            'category': 'Online Video Platforms'
        }, {
            'uid': 'jobvite',
            'name': 'Jobvite',
            'category': 'Recruitment'
        }, {
            'uid': 'litmos',
            'name': 'Litmos',
            'category': 'Education Management Software'
        }, {
            'uid': 'amadesa',
            'name': 'Amadesa',
            'category': 'Personalization'
        }, {
            'uid': 'ruby_on_rails',
            'name': 'Ruby On Rails',
            'category': 'Frameworks and Programming Languages'
        }, {
            'uid': 'new_relic',
            'name': 'New Relic',
            'category': 'Web Performance Monitoring'
        }, {
            'uid': 'nginx',
            'name': 'Nginx',
            'category': 'Load Balancers'
        }, {
            'uid': 'facebook_login',
            'name': 'Facebook Login (Connect)',
            'category': 'Social Login'
        }, {
            'uid': 'taleo',
            'name': 'Taleo',
            'category': 'Recruitment'
        }, {
            'uid': 'ubuntu',
            'name': 'Ubuntu',
            'category': 'Load Balancers'
        }, {
            'uid': 'vidyard',
            'name': 'Vidyard',
            'category': 'Online Video Platforms'
        }, {
            'uid': 'open_adstream_appnexus',
            'name': 'Open AdStream (Appnexus)',
            'category': 'Publisher Advertising Tools'
        }, {
            'uid': 'linkedin_display_ads__formerly_bizo',
            'name': 'Linkedin Marketing Solutions',
            'category': 'Advertising Networks'
        }, {
            'uid': 'google_font_api',
            'name': 'Google Font API',
            'category': 'Fonts'
        }, {
            'uid': 'google_adwords_conversion',
            'name': 'Google AdWords Conversion',
            'category': 'Advertising Networks'
        }, {
            'uid': 'zopim',
            'name': 'Zopim',
            'category': 'Chats'
        }, {
            'uid': 'multilingual',
            'name': 'Multilingual',
            'category': 'Widgets'
        }, {
            'uid': 'facebook_widget',
            'name': 'Facebook Widget',
            'category': 'Widgets'
        }, {
            'uid': 'icims',
            'name': 'iCIMS',
            'category': 'Recruitment'
        }, {
            'uid': 'mobile_friendly',
            'name': 'Mobile Friendly',
            'category': 'Other'
        }, {
            'uid': 'searchdex',
            'name': 'SearchDex',
            'category': 'Search Marketing'
        }, {
            'uid': 'facebook_web_custom_audiences',
            'name': 'Facebook Custom Audiences',
            'category': 'Retargeting'
        }]
    }
}

JOB_OPENING_RESPONSE = {
    'organization_job_postings': [{
        'id': '5fa32063272afb00015bb8c2',
        'title': 'Merchandiser',
        'url': 'https://pr.linkedin.com/jobs/view/merchandiser-at-apollo-2275621703?refId=a13b6e19-d0e5-4e05-8a17-821944837526&position=16&pageNum=25&trk=public_jobs_job-result-card_result-card_full-click',
        'city': 'Saint George',
        'state': 'South Carolina',
        'country': 'United States',
        'last_seen_at': '2020-11-04T21:42:58.928+00:00',
        'posted_at': '2020-11-04T18:42:58.928+00:00'
    }]
}

ENRICHED_PERSON_RESPONSE = {
    'person': {
        'id': '5fb68c77e7a4990001550e4a',
        'first_name': 'Tim',
        'last_name': 'Zheng',
        'name': 'Tim Zheng',
        'linkedin_url': 'http://www.linkedin.com/in/tim-zheng-677ba010',
        'title': 'Founder & CEO',
        'city': 'San Francisco',
        'email_status': 'verified',
        'photo_url': 'https://media-exp1.licdn.com/dms/image/C5603AQGiphGg4YXw4Q/profile-displayphoto-shrink_400_400/0?e=1611187200&v=beta&t=YaJx6pAWG7rnd0LrK4CI-gCyhU3vepY8NfY5FX2D-BQ',
        'twitter_url': None,
        'github_url': None,
        'facebook_url': None,
        'extrapolated_email_confidence': None,
        'headline': 'Founder & CEO at Apollo',
        'country': 'United States',
        'email': 'tzheng@apollo.io',
        'state': 'CA',
        'excluded_for_leadgen': False,
        'organization_id': '5e66b6381e05b4008c8331b8',
        'organization': {
            'id': '5e66b6381e05b4008c8331b8',
            'name': 'Apollo',
            'website_url': 'http://www.apollo.io',
            'blog_url': None,
            'angellist_url': None,
            'linkedin_url': 'http://www.linkedin.com/company/apolloio',
            'twitter_url': 'https://twitter.com/MeetApollo/',
            'facebook_url': 'https://www.facebook.com/MeetApollo/',
            'primary_phone': {},
            'languages': [],
            'alexa_ranking': 35558,
            'phone': None,
            'linkedin_uid': '18511550',
            'publicly_traded_symbol': None
        },
        'revealed_for_current_team': True
    }
}


class FakeApolloClient:
    def get_employees(self, domain, page):
        if domain == "test" and page == 1:
            return EMPLOYEE_RESPONSE
        elif domain == "error" and page == 1:
            self._raise_error()

    def get_enriched_person(self, first_name, last_name, org_name, domain, email):
        if first_name == "test" and last_name == "testl" and org_name == "to the stars academy" and domain == "test.com" and email == "test_email":
            return ENRICHED_PERSON_RESPONSE
        elif first_name == "error" and last_name == "error" and org_name == "error_org" and domain == "error" and email == "error_email":
            self._raise_error()

    def get_organization(self, domain):
        if domain == "test.io":
            return ORGANIZATION_RESPONSE
        elif domain == "error":
            self._raise_error()

    def get_job_openings(self, domain):
        if domain == "test.io":
            return JOB_OPENING_RESPONSE
        elif domain == "error":
            self._raise_error()
    
    def _raise_error(self, *args, **kwargs):
        raise Exception('Foo')


class TestApoloExtractor(TestCase):

    def setUp(self) -> None:
        self.fake_logger = Mock()
        self.fake_client = FakeApolloClient()
        self.extractor = ApolloExtractor(self.fake_client, self.fake_logger)

    def test_get_employees_returns_expected_response(self):
        actual = self.extractor.get_employees(domain="test", page=1)
        expected = {
            'employees': [Employee(
                firstName='Teddy',
                lastName='White',
                fullName='Teddy White',
                socialUrls=SocialFragment(
                    linkedinUrl="test linkedin",
                    twitterUrl="test twitter",
                    facebookUrl="test facebook",
                    githubUrl="test github",
                    photoUrl="test photo",
                    angellistUrl=None
                ),
                jobTitle='test_title',
                taglineDescription='test headline',
                address=Address(
                    city='San Francisco',
                    country='United States',
                    state='CA'
                ),
                organization=OrganizationFragment(
                    name="Google",
                    domain="http://www.google.com"
                )
            )],
            'currentPage': 1,
            'totalPages': 10
        }
        self.assertEqual(expected, actual)

    def test_get_enriched_person_returns_expected_response(self):
        actual = self.extractor.get_enriched_person(first_name = "test", last_name="testl", 
            org_name = "to the stars academy", domain = "test.com", email = "test_email")
        expected = EnrichedPerson(
            firstName='Tim', 
            lastName='Zheng', 
            fullName='Tim Zheng', 
            socialUrls=SocialFragment(
                linkedinUrl='http://www.linkedin.com/in/tim-zheng-677ba010', 
                photoUrl='https://media-exp1.licdn.com/dms/image/C5603AQGiphGg4YXw4Q/profile-displayphoto-shrink_400_400/0?e=1611187200&v=beta&t=YaJx6pAWG7rnd0LrK4CI-gCyhU3vepY8NfY5FX2D-BQ', 
                twitterUrl=None, 
                facebookUrl=None, 
                githubUrl=None, 
                angellistUrl=None), 
            title='Founder & CEO', 
            email='tzheng@apollo.io', 
            emailStatus='verified',
            address= Address(
                city='San Francisco', 
                country='United States', 
                state='CA'
                ),
            organization=OrganizationFragment(
                name='Apollo', 
                domain='http://www.apollo.io'))
        self.assertEqual(expected, actual)

    def test_get_organization_returns_expected_response(self):
        actual = self.extractor.get_organization(domain="test.io")
        expected = Organization(name='Apollo', domain='http://www.apollo.io', blog=None, socialUrls=SocialFragment(linkedinUrl='http://www.linkedin.com/company/apolloio', 
            photoUrl=None, twitterUrl='https://twitter.com/MeetApollo/', facebookUrl='https://www.facebook.com/MeetApollo/', githubUrl=None, angellistUrl=None), phoneNumber=None, languages=[], alexaRanking=35558, linkedinUid='18511550', stockSymbol=None, 
            stockExchange=None, logoLocation='https://zenprospect-production.s3.amazonaws.com/uploads/pictures/5f8a0d9ad1187e000139430e/picture', crunchbaseUrl=None, marketCap=None, industry='computer software', 
            descriptors=['sales engagement', 'lead generation', 'predictive analytics', 'lead scoring', 'sales strategy', 'conversation intelligence', 'sales enablement', 'lead routing', 'sales development', 'and email engagement'], numberOfEmployees=45, address= Address(address='535 Mission St, Suite 1100, San Francisco, California 94105, US', street='535 Mission St', city='San Francisco', state='California', country='United States', zipcode='94105'), 
            parentOrgIdentifier=None, suborganizations=[], numberOfSuborganizations=0, searchDescription='Apollo is an intelligent, data-first engagement platform that puts structured data at the core of your workflows to help you execute, analyze, and improve on your growth strategy.', 
            shortDescription="Apollo is the unified engagement acceleration platform that gives reps the ability to dramatically increase their number of quality conversations and opportunities. Reps are empowered to do more than just conduct outreach, they learn who to target, how to reach out, and what to say at speed and scale. We help drive growth and success by providing the means for teams to discover and utilize their organization's unique best practices. \n\nBy working in a unified platform, reps and managers alike save hours of time each day, strategy changes are instantly scaled across the whole team, and managers can finally dig into data at each step of their pipeline to continually find new ways to improve. \n\nTeams get access to our database of 200+ million contacts with a built-in fully customizable Scoring Engine, full sales engagement stack, our native Account Playbook builder, and the industry's only custom deep analytics suite. Managers create and enforce order and process with the industry's most advanced Rules Engine.\n\nApollo is the foundation for your entire end-to-end sales strategy.\n\nJoin teams like inDesign, Eden, and Gympass to experience the future of sales today. Ready to join our crew? Email sales@apollo.io.", 
            annualRevenue=None, techStackNames=['Cloudflare DNS', 'Rackspace MailGun', 'Gmail', 'Marketo', 'Google Apps', 'Microsoft Office 365', 'CloudFlare Hosting', 'Route 53', 'Zendesk', 'Stripe', 'Segment.io', 'Amplitude', 'Hubspot', 'MailChimp', 'Eloqua', 'Dropbox', 'eRecruit', 'Crelate', 'Front Desk Scheduling', 'ShareThis', 'AT Internet', 'Intercom', 'Act-On', 'Avangate', 'Eventbrite', 'Django', 'Inspectlet', 'Unreal Engine', 'PC Recruiter', 'PicReel', 'Intelligent Demand', 'Google Play', 'etouches', 'DoubleClick Conversion', 'Mixpanel', 'SilkRoad', 'Greenhouse.io', 'Bootstrap Framework', 'FullStory', 'Google Dynamic Remarketing', 'Lever', 'Google Tag Manager', 'iPerceptions', 'WordPress.org', 'Google Analytics', 'iTunes', 'Typekit', 'DoubleClick', 'Linkedin Login', 'Linkedin Widget', 'Wistia', 'Jobvite', 'Litmos', 'Amadesa', 'Ruby On Rails', 'New Relic', 'Nginx', 'Facebook Login (Connect)', 'Taleo', 'Ubuntu', 'Vidyard', 'Open AdStream (Appnexus)', 'Linkedin Marketing Solutions', 'Google Font API', 'Google AdWords Conversion', 'Zopim', 'Multilingual', 'Facebook Widget', 'iCIMS', 'Mobile Friendly', 'SearchDex', 'Facebook Custom Audiences'], currentTechnologies=[CurrentTechnology(uid='cloudflare_dns', name='Cloudflare DNS', category='Domain Name Services'), 
            CurrentTechnology(uid='rackspace_mailgun', name='Rackspace MailGun', category='Email Delivery'), CurrentTechnology(uid='gmail', name='Gmail', category='Email Providers'), CurrentTechnology(uid='marketo', name='Marketo', category='Marketing Automation'), CurrentTechnology(uid='google_apps', name='Google Apps', category='Other'), CurrentTechnology(uid='office_365', name='Microsoft Office 365', category='Other'), CurrentTechnology(uid='cloudflare_hosting', name='CloudFlare Hosting', category='Hosting'), CurrentTechnology(uid='route_53', name='Route 53', category='Domain Name Services'), CurrentTechnology(uid='zendesk', name='Zendesk', category='Support and Feedback'), CurrentTechnology(uid='stripe', name='Stripe', category='Payments'), CurrentTechnology(uid='segment_io', name='Segment.io', category='Analytics and Tracking'), CurrentTechnology(uid='amplitude', name='Amplitude', category='Analytics and Tracking'), CurrentTechnology(uid='hubspot', name='Hubspot', category='Marketing Automation'), CurrentTechnology(uid='mailchimp', name='MailChimp', category='Email Marketing'), CurrentTechnology(uid='eloqua', name='Eloqua', category='Marketing Automation'), CurrentTechnology(uid='dropbox', name='Dropbox', category='Cloud Services'), 
            CurrentTechnology(uid='erecruit', name='eRecruit', category='Recruitment'), CurrentTechnology(uid='crelate', name='Crelate', category='Recruitment'), CurrentTechnology(uid='front_desk_scheduling', name='Front Desk Scheduling', category='Appointment Scheduling'), CurrentTechnology(uid='sharethis', name='ShareThis', category='Social Sharing'), CurrentTechnology(uid='at_internet', name='AT Internet', category='Analytics and Tracking'), CurrentTechnology(uid='intercom', name='Intercom', category='Support and Feedback'), CurrentTechnology(uid='act-on', name='Act-On', category='Marketing Automation'), CurrentTechnology(uid='avangate', name='Avangate', category='Payments'), CurrentTechnology(uid='eventbrite', name='Eventbrite', category='Event Management'), CurrentTechnology(uid='django', name='Django', category='Frameworks and Programming Languages'), CurrentTechnology(uid='inspectlet', name='Inspectlet', category='Analytics and Tracking'), 
            CurrentTechnology(uid='unreal_engine', name='Unreal Engine', category='Other'), CurrentTechnology(uid='pc_recruiter', name='PC Recruiter', category='Recruitment'), CurrentTechnology(uid='picreel', name='PicReel', category='Site and Cart Abandonment'), CurrentTechnology(uid='intelligent_demand', name='Intelligent Demand', category='Lead Generation Software'), CurrentTechnology(uid='google_play', name='Google Play', category='Widgets'), CurrentTechnology(uid='etouches', name='etouches', category='Event Management'), CurrentTechnology(uid='doubleclick_conversion', name='DoubleClick Conversion', category='Advertising Networks'), CurrentTechnology(uid='mixpanel', name='Mixpanel', category='Analytics and Tracking'), CurrentTechnology(uid='silkroad', name='SilkRoad', category='Recruitment'), CurrentTechnology(uid='greenhouse_io', name='Greenhouse.io', category='Recruitment'), CurrentTechnology(uid='bootstrap_framework', name='Bootstrap Framework', category='CSS and JavaScript Libraries'), CurrentTechnology(uid='fullstory', name='FullStory', category='Other'), CurrentTechnology(uid='google_dynamic_remarketing', name='Google Dynamic Remarketing', category='Retargeting'), CurrentTechnology(uid='lever', name='Lever', category='Recruitment'), 
            CurrentTechnology(uid='google_tag_manager', name='Google Tag Manager', category='Tag Management'), CurrentTechnology(uid='iperceptions', name='iPerceptions', category='Support and Feedback'), CurrentTechnology(uid='wordpress_org', name='WordPress.org', category='CMS'), CurrentTechnology(uid='google_analytics', name='Google Analytics', category='Analytics and Tracking'), CurrentTechnology(uid='itunes', name='iTunes', category='Widgets'), CurrentTechnology(uid='typekit', name='Typekit', category='Fonts'), CurrentTechnology(uid='doubleclick', name='DoubleClick', category='Ad Servers'), CurrentTechnology(uid='linkedin_login', name='Linkedin Login', category='Social Login'), CurrentTechnology(uid='linkedin_widget', name='Linkedin Widget', category='Other'), CurrentTechnology(uid='wistia', name='Wistia', category='Online Video Platforms'), CurrentTechnology(uid='jobvite', name='Jobvite', category='Recruitment'), CurrentTechnology(uid='litmos', name='Litmos', category='Education Management Software'), CurrentTechnology(uid='amadesa', name='Amadesa', category='Personalization'), CurrentTechnology(uid='ruby_on_rails', name='Ruby On Rails', category='Frameworks and Programming Languages'), CurrentTechnology(uid='new_relic', name='New Relic', category='Web Performance Monitoring'), 
            CurrentTechnology(uid='nginx', name='Nginx', category='Load Balancers'), CurrentTechnology(uid='facebook_login', name='Facebook Login (Connect)', category='Social Login'), CurrentTechnology(uid='taleo', name='Taleo', category='Recruitment'), CurrentTechnology(uid='ubuntu', name='Ubuntu', category='Load Balancers'), CurrentTechnology(uid='vidyard', name='Vidyard', category='Online Video Platforms'), CurrentTechnology(uid='open_adstream_appnexus', name='Open AdStream (Appnexus)', category='Publisher Advertising Tools'), CurrentTechnology(uid='linkedin_display_ads__formerly_bizo', name='Linkedin Marketing Solutions', category='Advertising Networks'), CurrentTechnology(uid='google_font_api', name='Google Font API', category='Fonts'), CurrentTechnology(uid='google_adwords_conversion', name='Google AdWords Conversion', category='Advertising Networks'), CurrentTechnology(uid='zopim', name='Zopim', category='Chats'), 
            CurrentTechnology(uid='multilingual', name='Multilingual', category='Widgets'), CurrentTechnology(uid='facebook_widget', name='Facebook Widget', category='Widgets'), CurrentTechnology(uid='icims', name='iCIMS', category='Recruitment'), CurrentTechnology(uid='mobile_friendly', name='Mobile Friendly', category='Other'), CurrentTechnology(uid='searchdex', name='SearchDex', category='Search Marketing'), 
                    CurrentTechnology(uid='facebook_web_custom_audiences', name='Facebook Custom Audiences', category='Retargeting')])
        self.assertEqual(expected, actual)

    def test_get_job_openings_returns_expected_response(self):
        actual = self.extractor.get_job_openings(domain="test.io")
        expected = [JobOpening(title='Merchandiser', 
            listingUrl='https://pr.linkedin.com/jobs/view/merchandiser-at-apollo-2275621703?refId=a13b6e19-d0e5-4e05-8a17-821944837526&position=16&pageNum=25&trk=public_jobs_job-result-card_result-card_full-click', address= Address(city='Saint George', state='South Carolina', country='United States'), timePosted='2020-11-04T18:42:58.928+00:00')]
        self.assertEqual(expected, actual)

    def test_get_employees_exception__raises_expected_error(self):
        actual = self.extractor.get_employees(domain='error', page = 1)
        expected = {'errors': GC_GENERIC_ERROR_MESSAGE}
        self.assertEqual(expected, actual)

    def test_get_organization__raises_expected_error(self):
        actual = self.extractor.get_organization(domain='error')
        expected = {'errors': GC_GENERIC_ERROR_MESSAGE}
        self.assertEqual(expected, actual)

    def test_get_job_openings__raises_expected_error(self):
        actual = self.extractor.get_job_openings(domain='error')
        expected = {'errors': GC_GENERIC_ERROR_MESSAGE}
        self.assertEqual(expected, actual)
    
    def test_get_enriched_person__raises_expected_error(self):
        actual = self.extractor.get_enriched_person(
            first_name="error",
            last_name="error",
            org_name="error_org",
            domain="error",
            email="error_email"
        )
        expected = {'errors': GC_GENERIC_ERROR_MESSAGE}
        self.assertEqual(expected, actual)

    def test_get_employees__logs_expected_error(self):
        self.extractor.get_employees(domain='error', page=1)
        self.fake_logger.exception.assert_called_with('Foo')

    def test_get_enriched_person__logs_expected_error(self):
        self.extractor.get_enriched_person(
            first_name="error",
            last_name="error",
            org_name ="error_org",
            domain="error",
            email="error_email"
        )
        self.fake_logger.exception.assert_called_with('Foo')
    
    def test_get_organization__logs_expected_error(self):
        self.extractor.get_organization(domain='error')
        self.fake_logger.exception.assert_called_with('Foo')

    def test_get_job_openings__logs_expected_error(self):
        self.extractor.get_job_openings(domain='error')
        self.fake_logger.exception.assert_called_with('Foo')