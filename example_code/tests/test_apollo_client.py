from typing import List
from unittest import TestCase

from mock import Mock, patch, call

from backend.src.providers.apollo.client import ApolloClient

class TestApolloClient(TestCase):

    def setUp(self) -> None:
        self.fake_logger = Mock()
        self.fake_api_key = "test_key"
        self.client = ApolloClient(self.fake_api_key, self.fake_logger)
        self.client._get_org_identifier = Mock()
        self.client._get_org_identifier.return_value = "test_identifier"
        self.fake_request = Mock()
        self.patcher = patch('backend.src.providers.apollo.client.requests', self.fake_request)
        self.patcher.start()
        self.headers = {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache'
            }

    def tearDown(self) -> None:
        self.patcher.stop()
        return super().tearDown()

    def test_get_employees_makes_expected_request(self):
        self.client.get_employees(domain = "test", page = 1)
        self.fake_request.get.assert_called_with(
            url = "https://api.apollo.io/v1/mixed_people/search",
            params = {
                'api_key' : self.fake_api_key,
                "q_organization_domains": ['test'],
                "page" : 1
            },
            headers = self.headers
        )

    def test_get_enriched_person_makes_expected_request(self):
        self.client.get_enriched_person(first_name = "test_fname", last_name = "test_lname", organization_name = "test_oname" ,domain = "test", email= "test_email")
        self.fake_request.get.assert_called_with(
            url = "https://api.apollo.io/v1/people/match",
            params = {
                'api_key' : 'test_key',
                "first_name": 'test_fname',
                "last_name": 'test_lname',
                "organization_name": 'test_oname',
                "domain": 'test',
                "email": 'test_email'
            },
            headers = self.headers
        )

    def test_get_organization_makes_expected_request(self):
        self.client.get_organization(domain = "test")
        self.fake_request.get.assert_called_with(
            url = "https://api.apollo.io/v1/organizations/enrich",
            params = {
                'api_key' : "test_key",
                "domain": "test",
            },
            headers = self.headers
        )

    def test_get_job_openings_makes_expected_request(self):
        self.client.get_job_openings(domain = "test")
        self.fake_request.get.assert_called_with(
            url = "https://api.apollo.io/v1/organizations/test_identifier/job_postings",
            params = {
                'api_key' : "test_key",
            },
            headers = self.headers
        )

    