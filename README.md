# README #
### What is this repository for? ###

* This repository will be for commiting and reviewing work done by Felipe (FMG) on integrating datasources for General Commission.
### Project Description ###

* Build an integration for the hospital data found at the following URL:
    <https://data.cms.gov/provider-data/dataset/xubh-q36u>

* We want the following query to work: 
```
hospitalsBeta(name: String, address: String, zipcode: String, city: String): [Hospital]
```
* So want the inputs to be the variables above. The flow should look like this:
```
Inputs (name or address or zipcode or city) -> query the dataset for matching results -> Output(the schema provided in *hospital_compare.graphql*)
```
* At the bottom of the previous link you will find the endpoint to query the dataset using SQL syntax:
```
​/provider-data​/api​/1​/datastore​/sql?query=[SELECT * FROM 0fc6ab38-f8af-5539-acf8-ec98528f2d5a];
```
* We likely need to update this query with the input variables. So something like:
```
"SELECT * FROM uuid WHERE name = name, address = address"
```
* The following are the columns you will find in the dataset and a short description:
```
"xubh-q36u": {
"description": "A list of all hospitals that have been registered with Medicare. The list includes addresses,phone numbers,hospital type,and overall hospital rating.",
"schema": ["Facility ID", "Facility Name", "Address", "City", "State", "ZIP Code", "County Name", "Phone Number", "Hospital Type", "Hospital Ownership", "Emergency Services", "Meets criteria for promoting interoperability of EHRs", "Hospital overall rating", "Hospital overall rating footnote", "Mortality national comparison", "Mortality national comparison footnote", "Safety of care national comparison", "Safety of care national comparison footnote", "Readmission national comparison", "Readmission national comparison footnote", "Patient experience national comparison", "Patient experience national comparison footnote", "Effectiveness of care national comparison", "Effectiveness of care national comparison footnote", "Timeliness of care national comparison", "Timeliness of care national comparison footnote", "Efficient use of medical imaging national comparison", "Efficient use of medical imaging national comparison footnote"]
}
```

* I've included in this repository a file called *hospital_compare.graphql*, this file shows the schema we would like for our dataclasses. There is an example of a dataclass file (called dtos.py) in the example_code file
### Expected Deliverables ###
* **client.py**
    * The file responsible for making requests to the api endpoint
* **extractor.py**
    * The file responsible for taking the raw output of the api and translating it to our dataclasses
* **dtos.py**
    * The file that defines our dataclasses
* **factory.py**
    * The file that defines the function we can use to call your client/extractor in the rest of our codebase
*  **test_hospital_compare_client.py**
    *  unit tests for the hospital_compare client
*  **test_hospital_compare_extractor.py**
    *  unit tests for the hospital_compare extractor
* There are examples of each file in the example_code folder that are from a previous integration in our codebase. Your code may differ slightly because of the sql syntax but the examples should give you a basic idea of the structure

### Formatting ###

* Do your best to follow the formatting pattern in the example code
* Follow PEP8
* Please place all working code in a folder called hospital_compare_integration

### Who do I talk to? ###

* Reach Andrew Noel in our Upwork chat at anytime with questions. Please feel free to provide feedback or suggestions on how the project could be more clear.